package com.example;

import java.net.Socket;
import java.util.Scanner;
import java.net.*;
import java.io.*;

public class ClientMenu implements Runnable {

    private Peer peer;
    Socket socket;

    public ClientMenu(Peer peer) {
        this.peer = peer;
    }

    //TODO metodo para actualizar el peer con con nuevo archivo y se comunica con el server

    @Override
    public void run() {

        DataOutputStream output;
        BufferedInputStream bis;
        BufferedOutputStream bos;

        boolean flag = true;
        byte[] receivedData;

        String file;
        while (flag) {
            System.out.println(
                    "\r\nIngrese la opcion que desee:\r\n 1.Listar archivos disponibles\r\n 2.Buscar archivo\r\n3.Subir archivo\r\n4.Descargar \r\n0. Salir");
            Scanner scanner = new Scanner(System.in);

            String value = scanner.nextLine();
            // PEER: 10.10.10.1 - Archivo1 - PORT: 8090
            switch (value) {
                case "0":
                    flag = false;
                break;
                case "1":
                    break;
                case "2":
                    break;
                case "3":
                    break;
                case "4": //Descargar archivo
                    try {
                        //PRIMER PUNTO OBTENGO EL PEER con el puerto y el nombre del archivo
                        int in;
                        Socket socket = new Socket("localhost", 9090);
                        OutputStream outputStream = socket.getOutputStream();
                       
                        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

                        // Envio el nombre del archivo que deseo recibir
                        dataOutputStream.writeUTF("fede.txt");
                        dataOutputStream.flush(); // send the message

                        // Cierro el socket
                        outputStream.close();
                        dataOutputStream.close();

                        // Pongo a escuchar el peer para recibir el archivo
                        ServerSocket serverConec = new ServerSocket(9091);
                        Socket socket_server = serverConec.accept();
                        receivedData = new byte[1024];
                        bis = new BufferedInputStream(socket_server.getInputStream());
                        DataInputStream dis = new DataInputStream(socket_server.getInputStream());

                        // Recibo el nombre del archivo
                        file = dis.readUTF();

                        file = file.substring(file.indexOf('/') + 1, file.length());
                        // Comienzo a escribir el archivo
                        bos = new BufferedOutputStream(new FileOutputStream(file));
                        while ((in = bis.read(receivedData)) != -1) {
                            bos.write(receivedData, 0, in);
                        }
                        bos.close();
                        dis.close();

                    } catch (Exception e) {
                        // TODO: handle exception
                        System.out.println(e.getMessage());
                    }

                    break;

                default:
                    break;
            }
        }

    }

}
