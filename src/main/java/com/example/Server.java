package com.example;


class Server {
    /**
     * Falta
     * Server:
     * - Agregar un metodo para actualizar la lista de ficheros
     * 
     * Cliente:
     * - Poner el cliente a escuchar para la comunicacion p2p
     * - Metodo para solicitar lista actualizada de ficheros
     * - Metodo para solicitar fichero al siguiente peer
     * - Enviar fichero por p2p
     * 
     * 
     * @param args
     */
    public static void main(String[] args) {

        Listado listado;

        listado = new Listado();

        try {
            // Servidor Socket en el puerto 9000
            // Hilo que esucha consulta de todos los archivos EN 5000
            // Recibo String nombre de archivo
            ConsultasListado sh = new ConsultasListado(listado); // ,users)
            Thread consultasThread = new Thread(sh);
            consultasThread.start();

            // Hilo actualizar lista de archivos 9001
            // Recido peer con su lista de archivo dentro
            ActualizadorListado sh2 = new ActualizadorListado(listado); // ,users)
            Thread ActualizadorThread = new Thread(sh2);
            ActualizadorThread.start();

            // hIlo que escucha solicitudes de arhivos en el 9002
            // Recibo String nombre de archivo
            ServidorArchivos sh3 = new ServidorArchivos(listado); // ,users)
            Thread servidorArchivosThread = new Thread(sh3);
            servidorArchivosThread.start();

        } catch (Exception e) {
            System.err.println(e);
        }
    }
}