package com.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConsultasListado implements Runnable{

    Socket client;
    ServerSocket server;
    Listado listado;

    public ConsultasListado(Listado listado) {
        this.listado = listado;
    }

    @Override
    public void run() {
        ServerSocket ss;
        try {
            ss = new ServerSocket(9000);
            System.out.println("ConsultasListado: Estoy escuchado en 9000");

        while (true){
            Socket client = ss.accept();
            
            System.out.println("ConsultasListado: Atendiendo al cliente: "+client.getPort());

            HiloConsultasListado sh = new HiloConsultasListado(client, listado);
            // 2do paso
            Thread serverThread = new Thread(sh);
            // 3er paso
            serverThread.start();

        }
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

   
    }

}
